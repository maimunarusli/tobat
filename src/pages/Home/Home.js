import React, { Component } from 'react'
import { Image,TouchableOpacity, View } from 'react-native'

export class Home extends Component {
    render() {
        return (
            <View style={{flex:1}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#218F90'}}>
                    <Image source={require('../../assets/uu.png')} style={{width:160,height:65}} />
                </View>
                <View style={{flex:4,backgroundColor:'#218F90',alignItems:'center',}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Dzikir')}>
                        <Image source={require('../../assets/menuDzikir.png')} style={{width:180,height:140}} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Doa')}>
                        <Image source={require('../../assets/menuDoa.png')} style={{width:180,height:140}} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Asmaul Husna')}>
                        <Image source={require('../../assets/menuAsmaul.png')} style={{width:180,height:140}} />
                    </TouchableOpacity>
                    
                </View>
            </View>
        )
    }
}

export default Home
