import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'

export class Splash extends Component {

    componentDidMount(){
        setTimeout(() => {
            this.props.navigation.replace('Home')
        }, 3000)
    }

    render() {
        return (
            <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#218F90'}}>
                <Image source={require('../../assets/splash3.png')} style={{width:130,height:130}}/>
            </View>
        )
    }
}

export default Splash
