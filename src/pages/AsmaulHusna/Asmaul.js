import React, { Component } from 'react'
import {  View, Text, ScrollView} from 'react-native';
import { apiAsmaul } from '../../config'

export class Asmaul extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             data: [],
        }
    }

    componentDidMount(){
        this.getData()
    }

    getData = () => {
        apiAsmaul.get().then(hasil => {
            console.log(hasil.data.data);
            this.setState({
                data: hasil.data.data
            })
        })
    }

    
    
    render() {
        const {data} = this.state;
        return (
            <ScrollView style={{paddingHorizontal:5}}>
               {data.map((item,key) => {
                   return(
                       <View key={key} style={{width:'100%',backgroundColor:'#fff',marginVertical:5,padding:5,alignItems:'center'}}>
                           <Text >{item.arabic}</Text>
                           <Text>{item.latin}</Text>
                           <Text style={{textAlign:'center',fontStyle:'italic'}}>"{item.translation_id}"</Text>
                       </View>
                   )
               })}
            </ScrollView>
        )
    }
}

export default Asmaul
