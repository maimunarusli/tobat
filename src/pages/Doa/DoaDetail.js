import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class DoaDetail extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             arab: '',
             latin: '',
             judul: '',
             translate: '',
        }
    }

    componentDidMount(){
        this.getData();
    }

    getData = () => {
        const {route} = this.props;
        const {data} = route.params;
        // console.log(data);
        this.props.navigation.setOptions({
            title: data.title
        })
        this.setState({
            judul: data.title,
            arab: data.arabic,
            latin: data.latin,
            translate: data.translation
        })
    }
    
    render() {
        const {judul,arab,latin,translate} = this.state;
        return (
            <View style={{backgroundColor:'#fff',flex:1}}>
                <View style={{width:'100%',marginVertical:5,alignItems:'center'}}>
                    <Text style={{fontSize:20,fontWeight:'bold'}}>{judul}</Text>
                </View>
                <View style={{width:'100%',marginVertical:5,paddingHorizontal:10}}>
                    <Text style={{fontSize:24}}>{arab}</Text>
                </View>
                <View style={{width:'100%',marginVertical:5,paddingHorizontal:10}}>
                    <Text style={{fontSize:16,}}>{latin}</Text>
                </View>  
                <View style={{width:'100%',marginVertical:5,paddingHorizontal:10}}>
                    <Text style={{fontSize:16,fontStyle:'italic'}}>Artinya : "{translate}"</Text>
                </View>
            </View>
        )
    }
}

export default DoaDetail
