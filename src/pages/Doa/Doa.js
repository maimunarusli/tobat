import React, { Component } from 'react'
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { apiDoa } from '../../config'

export class Doa extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             data: [],
        }
    }

    componentDidMount(){
        this.getData();
    }
    
    getData = () => {
        apiDoa.get().then(hasil => {
            // console.log(hasil.data.data);
            this.setState({
                data: hasil.data.data,
            })
        })
    }

    render() {
        const {data} = this.state;
        return (
            <View>
                <ScrollView>
                {data.map((item,key) => {
                    return(
                        <TouchableOpacity key={key} onPress={() => this.props.navigation.navigate('Doa Detail',{data: item})}>
                            <View style={{width:'100%',height:50,backgroundColor:'#fff',borderBottomColor:'#000',borderWidth:0.2,justifyContent:'center',paddingHorizontal:10,}}>
                                <Text style={{fontWeight:'bold'}}>{item.title}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                })}
                </ScrollView>
            </View>
        )
    }
}

export default Doa
