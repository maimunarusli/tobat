import React from 'react'
import { View, Text } from 'react-native'

const CardDzikir = (props) => {
    return (
        <View>
            <View style={{width:'100%',padding:5}}>
                <Text style={{textAlign:'right',fontSize:18}}>
                    {props.arab}
                </Text>
            </View>
            <View style={{width:'100%',padding:5}}>
                <Text style={{fontSize:16,textAlign:'left'}}>
                    {props.keterangan}
                </Text>
            </View>
        </View>
    )
}

export default CardDzikir
