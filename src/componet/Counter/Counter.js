import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

const Counter = (props) => {
    return (
        <View style={{
            width:200,
            height:200,
            backgroundColor:'#093737',
            borderRadius:20,
            borderWidth:5,
            borderColor:'#AAE8E5'
        }}>
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <View style={{width:150,height:50,backgroundColor:'#fff',borderRadius:5,borderWidth:3,borderColor:'#218F90',justifyContent:'center',paddingHorizontal:5}}>
                    <Text style={{fontSize:32,fontWeight:'bold'}}>{props.hasil}</Text>
                </View>
            </View>
            <View style={{flex:1,flexDirection:'row'}}>
                <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                    <Text style={{fontWeight:'bold',color:'#fff',marginBottom:5}}>RESET</Text>
                    <TouchableOpacity onPress={props.reset}>
                        <View style={{width:25,height:25,backgroundColor:'#E5E5E5',borderRadius:50,borderWidth:3,borderColor:'#219293',marginRight:10}} />
                    </TouchableOpacity>
                </View>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontWeight:'bold',color:'#fff',marginBottom:5}}>COUNT</Text>
                    <TouchableOpacity onPress={props.Count}>
                        <View style={{width:60,height:60,backgroundColor:'#E5E5E5',borderRadius:50,borderWidth:3,borderColor:'#219293'}} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Counter
