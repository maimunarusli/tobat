import axios from "axios";

const apiDoa = axios.create({
    baseURL: 'https://islamic-api-zhirrr.vercel.app/api/doaharian',
});
const apiAsmaul = axios.create({
    baseURL: 'https://islamic-api-zhirrr.vercel.app/api/asmaulhusna',
});

export {apiDoa,apiAsmaul}
