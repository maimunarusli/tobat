import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import Splash from '../pages/Splash/Splash';
import Home from '../pages/Home/Home';
import Dzikir from '../pages/Dzikir/Dzikir';
import Doa from '../pages/Doa/Doa';
import Asmaul from '../pages/AsmaulHusna/Asmaul';
import DoaDetail from '../pages/Doa/DoaDetail';


const Stack =createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="Home"
                component={Home}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="Dzikir"
                component={Dzikir}
                options={{
                    headerStyle: {
                        backgroundColor: '#117B7C',
                    },
                    headerTintColor: '#fff',
                }}
            />
            <Stack.Screen
                name="Doa"
                component={Doa}
                options={{
                    headerStyle: {
                        backgroundColor: '#117B7C',
                    },
                    headerTintColor: '#fff',
                }}
            />
            <Stack.Screen
                name="Asmaul Husna"
                component={Asmaul}
                options={{
                    headerStyle: {
                        backgroundColor: '#117B7C',
                    },
                    headerTintColor: '#fff',
                }}
            />
            <Stack.Screen
                name="Doa Detail"
                component={DoaDetail}
                options={{
                    headerStyle: {
                        backgroundColor: '#117B7C',
                    },
                    headerTintColor: '#fff',
                }}
            />
        </Stack.Navigator>
    );
};

export default Route;